import { Component, OnInit } from '@angular/core';
import { trigger, transition, animate, style, state } from '@angular/animations';
import { slideInAnimation } from 'src/app/router-anmation';
import { RouterOutlet, Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [slideInAnimation]
})



export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  title = 'bigphelps-admin';
  _opened: boolean = false;
  submenu_data: any = [];

  _toggleSidebar() {
    this._opened = !this._opened;
  }
  submenu_services() {
    this._opened = !this._opened;
    this.submenu_data = [
      // { name: "Services", path: "service" },
    { name: "Services", path: "main-service" },
    { name: "Sub Services", path: "sub-service" }]
  }

  animation = ''

  getAnimation() {
    return this.animation;
  }

  
  getAnimationData() {
    // console.log("hihihiih")
    return this.router.url;
  }
  
}
