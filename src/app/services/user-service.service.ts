import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  constructor(private http: HttpClient) { }
  // services_url = "http://localhost:4000/user/services/";
  serviceUrl = "http://bigphelps.dxminds.online:4200/api/service/";
  // banner_url1 = "http://bigphelps.dxminds.online:4200/user/banner_image_homepage_single/";
  imageupload = "http://bigphelps.dxminds.online:4200/api/service/imageupload"

  imageUpload(image) {
    console.log(image)
    return this.http.post(this.imageupload, image)
  }

  getServiceList() {
    return this.http.get(this.serviceUrl);
  }


  getSingleService(id): Observable<any> {
    console.log(id)
    return this.http.get(this.serviceUrl + id);
  }

  addService(data): Observable<any> {
    console.log(data)
    return this.http.post(this.serviceUrl + 'create', data);
  }
  editService(id, data): Observable<any> {
    console.log(id, data)
    return this.http.put(this.serviceUrl + 'update/' + id, data);
  }
  deleteService(id): Observable<any> {
    return this.http.delete(this.serviceUrl+'delete/' + id);
  }


  update_servicedata(id, banner_data): Observable<any> {
    console.log(id, banner_data)
    return this.http.put(this.serviceUrl + id, banner_data);
  }

  delete_servicedata(id): Observable<any> {
    return this.http.delete(this.serviceUrl + id);
  }


  get_main_servicedata() {
    return this.http.get(this.serviceUrl);
  }
  get_single_main_servicedata(id): Observable<any> {
    console.log(id)
    return this.http.get(this.serviceUrl + id);
  }

  add_main_servicedata(data): Observable<any> {
    console.log(data)
    return this.http.post(this.serviceUrl, data);
  }

  update_main_servicedata(id, banner_data): Observable<any> {
    console.log(id, banner_data)
    return this.http.put(this.serviceUrl + id, banner_data);
  }

  delete_main_servicedata(id): Observable<any> {
    return this.http.delete(this.serviceUrl + id);
  }

}
