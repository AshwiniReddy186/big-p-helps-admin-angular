import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

 
  constructor(private http: HttpClient) { }
  banner_url = "http://bigphelps.dxminds.online:3000/user/banner/";


  get_bannerdata(): Observable<any> {
    return this.http.get(this.banner_url);
  }
  
  get_single_bannerdata(id): Observable<any> {
    console.log(id)
    return this.http.get(this.banner_url+id);
  }

  add_bannerdata(banner_data): Observable<any>{
    console.log(banner_data)
    return this.http.post(this.banner_url ,banner_data);
  }

  update_bannerdata(id,banner_data): Observable<any> {
    console.log(id,banner_data)
    return this.http.put(this.banner_url+id,banner_data);
  }

  delete_bannerdata(id) : Observable<any>{
    return this.http.delete(this.banner_url+id);
  }
}
