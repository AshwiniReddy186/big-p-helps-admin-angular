import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubServiceEditComponent } from './sub-service-edit.component';

describe('SubServiceEditComponent', () => {
  let component: SubServiceEditComponent;
  let fixture: ComponentFixture<SubServiceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubServiceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubServiceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
