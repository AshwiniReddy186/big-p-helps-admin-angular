import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubServiceAddComponent } from './sub-service-add.component';

describe('SubServiceAddComponent', () => {
  let component: SubServiceAddComponent;
  let fixture: ComponentFixture<SubServiceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubServiceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubServiceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
