import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubServiceHomeComponent } from './sub-service-home.component';

describe('SubServiceHomeComponent', () => {
  let component: SubServiceHomeComponent;
  let fixture: ComponentFixture<SubServiceHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubServiceHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubServiceHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
