import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ExcelService } from 'src/app/services/excel.service';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-main-service-home',
  templateUrl: './main-service-home.component.html',
  styleUrls: ['./main-service-home.component.css']
})
export class MainServiceHomeComponent implements OnInit {

  res_message;
  service_data: any = [];
  subscription: Subscription;
  showLoader=false;

  constructor(private user_service: UserServiceService,
    private excelService: ExcelService) {
  }

  ngOnInit() {
    this.getdata();
  }


  getdata() {
    this.showLoader=true;
    this.subscription = this.user_service.getServiceList().subscribe((data: any) => {
      console.log(data)
      this.showLoader=false;
      if (data.status == "success") {
        this.service_data = data.data;
        this.res_message = data.message;
      } else {
        this.res_message = data.message;
      }
    })
  }

  delete_main_service(id) {
  
    if (confirm('Are you sure to delete this record ?') == true) {
      this.showLoader=true;
      this.user_service.deleteService(id).subscribe((data: any) => {
        console.log(data);
        this.showLoader=false;
        if (data.status == "success") {
          this.getdata();
        }
        else {
          console.log(data)
        }
      })
    }
  }


  update(data, status) {
    console.log(data,status)

    this.user_service.editService(data._id, {
      "name": data.name,
      "vicinity": data.vicinity,
      "type":data.type,
      "imageUrl": data.image,
      "status": status == true ? 1 : 0,
    }).subscribe((data: any) => {
      console.log(data)
      if (data.status == "success") {
        this.getdata();
        this.res_message = data.message;
      } else {
        this.res_message = data.message;
      }
    })
  }



  exportAsXLSX() {
    this.excelService.exportAsExcelFile(this.service_data, 'main-service-list');
  }

  // ngOnDestroy() {
  //   // if(this.subscription){
  //   //   this.subscription.unsubscribe();
  //   // }

  // }


}
