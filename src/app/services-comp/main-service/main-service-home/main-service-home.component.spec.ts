import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainServiceHomeComponent } from './main-service-home.component';

describe('MainServiceHomeComponent', () => {
  let component: MainServiceHomeComponent;
  let fixture: ComponentFixture<MainServiceHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainServiceHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainServiceHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
