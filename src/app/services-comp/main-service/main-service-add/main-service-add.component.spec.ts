import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainServiceAddComponent } from './main-service-add.component';

describe('MainServiceAddComponent', () => {
  let component: MainServiceAddComponent;
  let fixture: ComponentFixture<MainServiceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainServiceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainServiceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
