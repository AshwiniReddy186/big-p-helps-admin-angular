import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserServiceService } from 'src/app/services/user-service.service';
// import { UserServiceService } from 'src/app/services/user-service.service';


@Component({
  selector: 'app-main-service-add',
  templateUrl: './main-service-add.component.html',
  styleUrls: ['./main-service-add.component.css']
})
export class MainServiceAddComponent implements OnInit {

  form: FormGroup;
  submitted = false;
  addmessage;
  myFiles;
  showerror = false;
  url;
  showimage = false;
  serviceData: any = [];
  vicinityData: any = [];

  serviceType = [
    { id: 1, value: 'Home' },
    { id: 2, value: 'Business' },
  ];
  vicinityType = [
    { id: 1, value: 'Outdoor' },
    { id: 2, value: 'Indoor' },
  ];


  constructor(private fb: FormBuilder, private user_service: UserServiceService, private router: Router) { }

  ngOnInit() {

    let serviceGroup = new FormArray(this.serviceType.map(item => new FormGroup({
      id: new FormControl(item.id),
      value: new FormControl(item.value),
      checkbox: new FormControl(false)
    })));

    let vicinityGroup = new FormArray(this.vicinityType.map(item => new FormGroup({
      id: new FormControl(item.id),
      value: new FormControl(item.value),
      checkbox: new FormControl(false)
    })));


    let hiddenControlService = new FormControl(this.mapService(serviceGroup.value), Validators.required);
    let hiddenControlVicinity = new FormControl(this.mapVicinity(vicinityGroup.value), Validators.required);


    serviceGroup.valueChanges.subscribe((v) => {
      console.log(v);
      hiddenControlService.setValue(this.mapService(v));
      hiddenControlVicinity.setValue(this.mapVicinity(v));
    });


    this.form = this.fb.group({
      type: serviceGroup,
      selectedService: hiddenControlService,
      vicinity: vicinityGroup,
      selectedType: hiddenControlVicinity,
      status: [1, [Validators.required]],
      name: ['', [Validators.required]],
      image: ['', [Validators.required]],
    });
  }

  mapService(items) {
    let selectedService = items.filter((item) => item.checkbox).map((item) => item.id);
    return selectedService.length ? selectedService : null;
  }

  mapVicinity(items) {
    let selectedVicinity = items.filter((item) => item.checkbox).map((item) => item.id);
    return selectedVicinity.length ? selectedVicinity : null;
  }


  get f() { return this.form.controls; }




  //submit form
  onSubmit(data) {

    console.log(data.value);
    var formData = data.value

    var servicetype = data.value.type.filter(
      data => data.checkbox === true);
    var vicinitytype = data.value.vicinity.filter(
      data => data.checkbox === true);

    servicetype.forEach(element => {
      this.serviceData.push(element.value);
    });
    vicinitytype.forEach(element => {
      this.vicinityData.push(element.value);
    });

    this.submitted = true;

    if (this.form.invalid) {
      return false;

    } else {

      const frmData = new FormData();
      frmData.append("image", this.myFiles);

      //image upload api
      this.user_service.imageUpload(frmData).subscribe((data: any) => {

        if (data.status == "success") {

          this.user_service.addService({
            "name": formData.name,
            "type": this.serviceData,
            "vicinity": this.vicinityData,
            "imageUrl": data.data[0].imageUrl,
            "status": formData.status
          }).subscribe((data: any) => {

            console.log(data)
            if (data.status == "success") {
              console.log(data.status);
              this.router.navigate(['/home',{outlets:{menucontent:['main-service']}}]);
            }
            else {
              this.showerror=true;
              this.addmessage=data.message;
              console.log(data.status)
            }
          });

        }
        else {
          this.showerror=true;
          this.addmessage=data.message;
          console.log(data.status)
        }
      });

    }
  }





  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.myFiles = event.target.files[0];
      this.showimage = true;
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      }
    }
  }





  alert_close() {
    this.url = "";
    this.form.reset();
  }


}
