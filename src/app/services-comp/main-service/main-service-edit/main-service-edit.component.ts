import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { UserServiceService } from 'src/app/services/user-service.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-service-edit',
  templateUrl: './main-service-edit.component.html',
  styleUrls: ['./main-service-edit.component.css']
})
export class MainServiceEditComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  addmessage;
  myFiles;
  showerror = false;
  url;
  showimage = false;
  sub;
  id;
  subscription: Subscription;
  vicinityType: any[] = [];
  serviceType: any[] = [];
  editImageUrl;
  vicinityType_res: any[] = [];
  serviceType_res: any[] = [];
  serviceData: any = [];
  vicinityData: any = [];
  showLoader = false;

  constructor(private fb: FormBuilder, private user_service: UserServiceService,
    private ActivatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.sub = this.ActivatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
    });


    this.getData();

    let vicinityGroup = this.vicinityType.map(x => {
      console.log(x)
      return this.fb.group({
        id: x.id,
        name: x.name,
        checkbox: x.checkbox
      });
    });

    let serviceGroup = this.serviceType.map(x => {
      return this.fb.group({
        id: x.id,
        name: x.name,
        checkbox: x.checkbox
      });
    });

    this.form = this.fb.group({
      type: this.fb.array(serviceGroup),
      vicinity: this.fb.array(vicinityGroup),
      status: [, [Validators.required]],
      name: ['', [Validators.required]],
      new_image: ['',],
      old_image: ['']
    });

  }




  //get single data
  getData() {
    this.showLoader=true;
    this.vicinityType = [
      { id: 1, name: 'Outdoor', checkbox: 'false' },
      { id: 2, name: 'Indoor', checkbox: 'false' },
    ];
    this.serviceType = [
      { id: 1, name: 'Home', checkbox: 'false' },
      { id: 2, name: 'Business', checkbox: 'false' },
    ];


    this.subscription = this.user_service.getSingleService(this.id).subscribe((data: any) => {
      console.log(data)
      this.showLoader = false;
      if (data.status == "success") {
        this.form.controls['status'].setValue(data.data.status);
        this.form.controls['name'].patchValue(data.data.name);
        this.form.controls['old_image'].patchValue(data.data.imageUrl);
        this.url = data.data.imageUrl;

        data.data.vicinity.forEach(x => {
          this.vicinityType.forEach(data => {
            if (data.name == x) {
              data.checkbox = true;
            }
            else {
              data.checkbox = false;
            }
            this.vicinityType_res.push(data);
            this.form.get('vicinity').patchValue(this.vicinityType_res);
          })
        })

        data.data.type.forEach(x => {
          this.serviceType.forEach(data => {
            if (data.name == x) {
              data.checkbox = true;
            }
            else {
              data.checkbox = false;
            }
            this.serviceType_res.push(data);
            this.form.get('type').patchValue(this.serviceType_res);
          })
        })
      }
      else {
        this.showerror = true;
        this.addmessage = data.message;
      }

    });
  }






  get f() { return this.form.controls; }





  onSubmit(data) {
    this.submitted = true;
    console.log(data.value);
    var submitData = data.value;

    var servicetype = data.value.type.filter(
      data => data.checkbox === true);

    var vicinitytype = data.value.vicinity.filter(
      data => data.checkbox === true);

    servicetype.forEach(element => {
      this.serviceData.push(element.name);
    });
    vicinitytype.forEach(element => {
      this.vicinityData.push(element.name);
    });


    if (this.form.invalid) {
      return false;
    } else {
      const frmData = new FormData();

      if (this.myFiles != undefined) {
        frmData.append("image", this.myFiles);

        this.user_service.imageUpload(frmData).subscribe((data: any) => {
          this.editImageUrl = data.data[0].imageUrl;

          this.user_service.editService(this.id, {
            "name": submitData.name,
            "type": this.serviceData,
            "vicinity": this.vicinityData,
            "imageUrl": this.editImageUrl,
            "status": submitData.status
          }).subscribe((data: any) => {
            console.log(data);
            if (data.status == "success") {
              this.addmessage = data.message;
              this.router.navigate(['/home', { outlets: { menucontent: ['main-service'] } }]);
            } else {
              this.showerror = true;
              this.addmessage = data.message;
            }
          })
        });

      } else {
        this.editImageUrl = submitData.old_image;
        this.user_service.editService(this.id, {
          "name": submitData.name,
          "type": this.serviceData,
          "vicinity": this.vicinityData,
          "imageUrl": this.editImageUrl,
          "status": submitData.status
        }).subscribe((data: any) => {
          console.log(data)
          if (data.status == "success") {
            this.addmessage = data.message;
            this.router.navigate(['/home', { outlets: { menucontent: ['main-service'] } }]);
          } else {
            this.showerror = true;
            this.addmessage = data.message;
          }
        });

      }

    }


  }


  onSelectFile(event) {
    console.log(event.target.files[0])
    if (event.target.files && event.target.files[0]) {
      this.myFiles = event.target.files[0];
      this.showimage = true;
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      }
    }
  }





  alert_close() {
    this.url = "";
    this.form.reset();
  }

  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }
}






