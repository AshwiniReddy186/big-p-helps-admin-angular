import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainServiceEditComponent } from './main-service-edit.component';

describe('MainServiceEditComponent', () => {
  let component: MainServiceEditComponent;
  let fixture: ComponentFixture<MainServiceEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainServiceEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainServiceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
