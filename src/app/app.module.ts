import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarModule } from 'ng-sidebar';
import { TopNavbarComponent } from './navbar/top-navbar/top-navbar.component';
import { FirstSideNavbarComponent } from './navbar/first-side-navbar/first-side-navbar.component';
import { SecondSideNavbarComponent } from './navbar/second-side-navbar/second-side-navbar.component';
import { BannerHomeComponent } from './banner/banner-home/banner-home.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SearchPipe } from './pipes/search.pipe';
import { BannerAddComponent } from './banner/banner-add/banner-add.component';
import { BannerEditComponent } from './banner/banner-edit/banner-edit.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';

import { MainServiceHomeComponent } from './services-comp/main-service/main-service-home/main-service-home.component';
import { MainServiceAddComponent } from './services-comp/main-service/main-service-add/main-service-add.component';
import { MainServiceEditComponent } from './services-comp/main-service/main-service-edit/main-service-edit.component';
import { LoginComponent } from './authentication/login/login.component';
import { HomeComponent } from './homepage/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SubServiceHomeComponent } from './services-comp/sub-service/sub-service-home/sub-service-home.component';
import { SubServiceAddComponent } from './services-comp/sub-service/sub-service-add/sub-service-add.component';
import { SubServiceEditComponent } from './services-comp/sub-service/sub-service-edit/sub-service-edit.component';



@NgModule({
  declarations: [
    AppComponent,
    TopNavbarComponent,
    FirstSideNavbarComponent,
    SecondSideNavbarComponent,
    BannerHomeComponent,
    SearchPipe,
    BannerAddComponent,
    BannerEditComponent,
    DashboardHomeComponent,
    MainServiceHomeComponent,
    MainServiceAddComponent,
    MainServiceEditComponent,
    LoginComponent,
    HomeComponent,
    SubServiceHomeComponent,
    SubServiceAddComponent,
    SubServiceEditComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule, SidebarModule.forRoot(), NgxPaginationModule, UiSwitchModule, HttpClientModule,
    ReactiveFormsModule, FormsModule,BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
