import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondSideNavbarComponent } from './second-side-navbar.component';

describe('SecondSideNavbarComponent', () => {
  let component: SecondSideNavbarComponent;
  let fixture: ComponentFixture<SecondSideNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondSideNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondSideNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
