import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstSideNavbarComponent } from './first-side-navbar.component';

describe('FirstSideNavbarComponent', () => {
  let component: FirstSideNavbarComponent;
  let fixture: ComponentFixture<FirstSideNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstSideNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstSideNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
