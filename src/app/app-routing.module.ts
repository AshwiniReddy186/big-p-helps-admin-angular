import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BannerHomeComponent } from './banner/banner-home/banner-home.component';
import { BannerAddComponent } from './banner/banner-add/banner-add.component';
import { BannerEditComponent } from './banner/banner-edit/banner-edit.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { MainServiceEditComponent } from './services-comp/main-service/main-service-edit/main-service-edit.component';
import { MainServiceHomeComponent } from './services-comp/main-service/main-service-home/main-service-home.component';
import { MainServiceAddComponent } from './services-comp/main-service/main-service-add/main-service-add.component';
import { LoginComponent } from './authentication/login/login.component';
import { HomeComponent } from './homepage/home/home.component';
import { SubServiceHomeComponent } from './services-comp/sub-service/sub-service-home/sub-service-home.component';
import { SubServiceAddComponent } from './services-comp/sub-service/sub-service-add/sub-service-add.component';
import { SubServiceEditComponent } from './services-comp/sub-service/sub-service-edit/sub-service-edit.component';


const routes: Routes = [

  {
    path: "",
    component: LoginComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "home",
    component: HomeComponent,
    children: [
      {
        path: "",
        component: BannerHomeComponent,
        outlet: "menucontent",
        data: { animation: 'one'}
      },
      {
        path: "banner",
        component: BannerHomeComponent,
        outlet: "menucontent",
        data: { animation: 'one'}
      },
      {
        path: "banner-add",
        component: BannerAddComponent,
        outlet: "menucontent"
      },
      {
        path: "banner-edit/:id",
        component: BannerEditComponent,
        outlet: "menucontent"
      },
      {
        path: "sub-service",
        component: SubServiceHomeComponent,
        outlet: "menucontent"
      },
      {
        path: "sub-service-add",
        component: SubServiceAddComponent,
        outlet: "menucontent"
      },
      {
        path: "sub-service-edit/:id",
        component: SubServiceEditComponent,
        outlet: "menucontent"
      },
      {
        path: "main-service",
        component: MainServiceHomeComponent,
        outlet: "menucontent"
      },
      {
        path: "main-service-add",
        component: MainServiceAddComponent,
        outlet: "menucontent"
      },
      {
        path: "main-service-edit/:id",
        component: MainServiceEditComponent,
        outlet: "menucontent"
      },
      {
        path: "dashboard",
        component: DashboardHomeComponent,
        outlet: "menucontent"
      },
    ]
  },

  // {
  //   path: "banner",
  //   component: BannerHomeComponent
  // },
  // {
  //   path: "banner-add",
  //   component: BannerAddComponent
  // },
  // {
  //   path: "banner-edit/:id",
  //   component: BannerEditComponent
  // },
  // {
  //   path: "service",
  //   component: ServiceHomeComponent
  // },
  // {
  //   path: "service-add",
  //   component: ServiceAddComponent
  // },
  // {
  //   path: "service-edit/:id",
  //   component: ServiceEditComponent
  // },
  // {
  //   path: "main-service",
  //   component: MainServiceHomeComponent
  // },
  // {
  //   path: "main-service-add",
  //   component: MainServiceAddComponent
  // },
  // {
  //   path: "main-service-edit/:id",
  //   component: MainServiceEditComponent
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
