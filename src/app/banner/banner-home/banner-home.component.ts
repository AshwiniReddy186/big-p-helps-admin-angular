import { Component, OnInit, OnDestroy } from '@angular/core';
import { BannerService } from '../../services/banner.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { Banner } from './banner.model';
// imimport { ExcelService } from '../../services/excel.service';
import { ExcelService } from '../../services/excel.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-banner-home',
  templateUrl: './banner-home.component.html',
  styleUrls: ['./banner-home.component.css']
})
export class BannerHomeComponent implements OnInit {

  getmessage;
  banner_data: Banner[] = [];
  banner_form: FormGroup;
  subscription: Subscription;
  addmessage: any;
  banner_status;

  constructor(private banner_service: BannerService, private fb: FormBuilder,
    private excelService: ExcelService,private router:Router,private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.banner_form = this.fb.group({
      status: ['']
    })
    this.getdata();
  }


  getdata() {
    console.log("hihiijh")
    this.subscription = this.banner_service.get_bannerdata().subscribe((data: any) => {
      console.log(data)
      if (data.status == "success") {
        this.banner_data = data.data;
        this.getmessage = data.message;
      } else {
        this.getmessage = data.message;
      }
    })
  }

  delete_bannerdata(id) {
    console.log(id)
    if (confirm('Are you sure to delete this record ?') == true) {
      this.banner_service.delete_bannerdata(id).subscribe((data: any) => {
        console.log(data);
        if (data.status == "success") {
          this.getdata();
        }
        else {
          console.log(data)
        }
      })
    }
  }


  update(data, status) {
    const frmData = new FormData();
    this.banner_status = 1;
    frmData.append("old_image", data.image);
    frmData.append("image_path", data.image_path);
    frmData.append("status", status == true ? '1' : '0');
    this.banner_service.update_bannerdata(data.id, frmData).subscribe((data: any) => {
      if (data.status == "success") {
        this.getdata();
        this.addmessage = data.message;
      } else {
        this.addmessage = data.message;
      }
    })
  }



  exportAsXLSX() {
    this.excelService.exportAsExcelFile(this.banner_data, 'Banner');
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }



  navigate(path) {
    this.router.navigate([{outlets: {primary: 'banner-add', sidemenu:"banner-add"}}], 
                         {relativeTo: this.route});
}

}
