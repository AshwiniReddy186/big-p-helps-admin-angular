export class Banner {
    _id: string;
    banner_description: String;
    banner_status: String;
    banner_image: String;
    banner_image_path: String;
}