import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BannerService } from '../../services/banner.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-banner-add',
  templateUrl: './banner-add.component.html',
  styleUrls: ['./banner-add.component.css']
})
export class BannerAddComponent implements OnInit {
  @ViewChild('fileInput', { static: true }) inputFile;
  banner_form: FormGroup;
  submitted = false;
  myFiles: any;
  addmessage;
  url;
  showimage = false;
  showerror = false;
  constructor(private fb: FormBuilder, private banner_service: BannerService, private router: Router) { }

  ngOnInit() {
    this.banner_form = this.fb.group({
      // bannerdescription: ['', [Validators.required]],
      status: [1, [Validators.required]],
      image: ['', [Validators.required]]
    })
  }


  get f() { return this.banner_form.controls; }

  onSubmit(data) {
    this.submitted = true;
    if (this.banner_form.invalid) {
      return false;
    } else {
      const frmData = new FormData();
      frmData.append("image", this.myFiles);
      frmData.append("status", this.banner_form.value.status);
      // console.log(frmData)
      // frmData.append("banner_description", this.banner_form.value.banner_description);
      this.banner_service.add_bannerdata(frmData).subscribe((data: any) => {
        console.log(data)
        if (data.status == "success") {
          // this.router.navigate(['/banner']);
          this.addmessage = data.message;
        } else {
          this.showerror = true;
          this.addmessage = data.message;
        }
      })
    }
  }

  onSelectFile(event) {
    console.log(event.target.files[0])
    if (event.target.files && event.target.files[0]) {
      this.myFiles = event.target.files[0];
      this.showimage = true;
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      }
    }
  }


  alert_close() {
    this.banner_form.reset();
  }


}
