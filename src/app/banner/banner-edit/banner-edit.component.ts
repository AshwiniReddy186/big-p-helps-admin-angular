import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BannerService } from 'src/app/services/banner.service';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-banner-edit',
  templateUrl: './banner-edit.component.html',
  styleUrls: ['./banner-edit.component.css']
})
export class BannerEditComponent implements OnInit {
 
  @ViewChild('fileInput', { static: true }) inputFile;
  banner_edit_form: FormGroup;
  myFiles: any;
  addmessage;
  sub;
  id;
  url;
  banner_image_path;
  subscription: Subscription;


  constructor(private fb: FormBuilder, private banner_service: BannerService,
    private ActivatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.banner_edit_form = this.fb.group({
      status: ['', [Validators.required]],
      new_image: ['',],
      old_image: ['']
    })

    this.sub = this.ActivatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
    });

    this.getdata();
  }



  get f() { return this.banner_edit_form.controls; }


  getdata() {
    this.subscription=this.banner_service.get_single_bannerdata(this.id).subscribe((data: any) => {
      console.log(data)
      const result = data.data[0];
      this.banner_edit_form.controls['status'].setValue(result.status);
      this.banner_edit_form.controls['old_image'].setValue(result.image);
      this.url = result.image_path;
    })
  }


  onSubmit(data) {
    const frmData = new FormData();
    frmData.append("image", this.myFiles);
    frmData.append("old_image", this.banner_edit_form.value.old_image);
    frmData.append("image_path", this.url);
    frmData.append("status", this.banner_edit_form.value.status);
    this.banner_service.update_bannerdata(this.id, frmData).subscribe((data: any) => {
      console.log(data)
      if (data.status == "success") {
        this.router.navigate(['/banner'])
        this.addmessage = data.message;
      } else {
        this.addmessage = data.message;
      }
    })
  }


  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.myFiles = event.target.files[0];
      console.log(this.myFiles)
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      }
    }
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}