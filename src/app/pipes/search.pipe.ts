import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {
  transform(items: any[], field: any, value: string): any[] {
    if (!items) return [];
    if (typeof field == 'string') {
      console.log("hi",items)
      let rtItems: any = items;
      try {
        rtItems = items.filter(it => it[field].toLowerCase().indexOf(value.toLowerCase()) > -1);
      } finally {
        return rtItems;
      }
    } else {
      console.log("hello",items)
      let rtItems: any = items;
      try {
        rtItems = items.filter(it => {
          for (let f of field) {
            if (it[f].toLowerCase().indexOf(value.toLowerCase()) > -1) {
              return true;
            }
          }
        });
      } finally {
        return rtItems;
      }
    }


  }

  // transform(arr: any[], value: any): any {
  //   if (arr) {
  //     if (!value) {
  //      console.log(arr)
  //     } else {
  //       return arr.filter(obj => this.filter(obj[prop], value))
  //     }
  //   } else {
  //     return []
  //   }
  // }

  // filter(source: string, target: string) {
  //   if (source.includes(target)) {
  //     return true;
  //   }

  // }


  // transform(value: any, args?: any): any {
  //   if (!args) {
  //     return value;
  //   } else {

  //     args = args

  //   }
  //    return  value.filter((el) => {
  //      console.log(el)

  //      const data=el.toLowerCase()
  //       if(data.includes(args)){
  //         return true;
  //       } else {        
  //         return false;
  //       }
  //     });
  //     }
  //     if(!value)return null;
  //     if(!args)return value;

  //     args = args.toLowerCase();

  //     return value.filter(function(item){
  //         return JSON.stringify(item).toLowerCase().includes(args);
  //     });
  // }
}
